package com.example.seckitchen;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;

public class GenKeysBC {
    private static final String TAG = GenKeysBC.class.getSimpleName();
    public GenKeysBC(){}

    void EncryptKeyforAes128(String pp, int i)  {
        //Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        KeyGenerator generator = null;
        try {
            generator = KeyGenerator.getInstance("AES", "BC");

        generator.init(128);

        Key keyToBeWrapped = generator.generateKey();

        System.out.println("input    : " + new String(keyToBeWrapped.getEncoded()));
        Cipher cipher = Cipher.getInstance("AESWrap", "BC");

        KeyGenerator KeyGen = KeyGenerator.getInstance("AES", "BC");
        KeyGen.init(256);

        Key wrapKey = KeyGen.generateKey();
        cipher.init(Cipher.WRAP_MODE, wrapKey);
        byte[] wrappedKey = cipher.wrap(keyToBeWrapped);

        System.out.println("wrapped : " + new String(wrappedKey));

        cipher.init(Cipher.UNWRAP_MODE, wrapKey);
        Key key = cipher.unwrap(wrappedKey, "AES", Cipher.SECRET_KEY);
        System.out.println("unwrapped: " + new String(key.getEncoded()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }
    public SecretKey forAES (String alias, int size){
        KeyGenerator KeyGen = null;
        try {
            KeyGen = KeyGenerator.getInstance("AES", "BC");
            KeyGen.init(256);
            SecretKey key = KeyGen.generateKey();
            return key;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return null;
        //return genSymmetricKey(AES, createAESKeyGenParam (alias, size));
    }



    public SecretKey forARC4 (String alias, int size){
        KeyGenerator KeyGen = null;
        try {
            KeyGen = KeyGenerator.getInstance("ARC4", "BC");
            KeyGen.init(size);
            SecretKey key = KeyGen.generateKey();
            return key;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return null;
        //return genSymmetricKey(AES, createAESKeyGenParam (alias, size));
    }
}
