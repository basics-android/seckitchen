package com.example.seckitchen;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class Pkcs12 {
    private Context c;

    private InputStream p12;
    private String pkcs12Resouce;
    private String pkcs12PassWord;
    private String alias;
    private KeyStore ks;
    X509Certificate x509Cert;
    private static final String TAG = Pkcs12.class.getSimpleName();
    public Pkcs12(String fileName, String pkcs12Pass, Context context){
        this.c = context;
        this.pkcs12Resouce = fileName;
        this.pkcs12PassWord = pkcs12Pass;

        AssetManager aManager = c.getAssets();
        try {
            p12 = aManager.open(pkcs12Resouce);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void printCert(String filename){
        try {
            File cert = new File("mnt/sdcard/" + filename + ".p12");
            InputStream inputStreamFromDownload = null;
            ks = KeyStore.getInstance("PKCS12");
            inputStreamFromDownload = new BufferedInputStream(new FileInputStream(cert));
            Log.i("Certificate", inputStreamFromDownload.available() + "");
        } catch (KeyStoreException | FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void loadPkcs12(){
        try {
            ks = KeyStore.getInstance("PKCS12");
            ks.load(p12, pkcs12PassWord.toCharArray());
            getAliasFromPKCS12();
        } catch (KeyStoreException ex) {
            System.err.println(" Error al generar el KeyStore PKCS12 ...)");
        } catch (NoSuchAlgorithmException ex) {
            System.err.println(" Error al generar el KeyStore PKCS12 ...)");
        } catch (CertificateException ex) {
            System.err.println("... Error al generar el KeyStore PKCS12 ...)");
        } catch (IOException ex) {
            System.err.println("... Error al generar el KeyStore PKCS12 ...)");
        }
    }

    public void getAliasFromPKCS12(){
        try {
            Enumeration e = ks.aliases();
            while (e.hasMoreElements()) {
                alias = (String) e.nextElement();
                Log.d("getCertsFromP12","alias:" + alias);
                x509Cert = (X509Certificate) ks.getCertificate(alias);
                // addCertificateToKeyStore(c);
            }
        } catch (Exception e) {}
    }

    private void addCertificateToKeyStore(X509Certificate c) {
        try {
            KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
            ks.load(null);
            ks.setCertificateEntry("myCertAlias", c);
        } catch (Exception e){}
    }
    public void loadPrivateKey(){
//        java.security.KeyStore keyStore = KeyStore.getInstance("PKCS12", "BC");
//        keyStore.load(inputStream, password);
        try {
            PrivateKey privateKey = (PrivateKey) ks.getKey(alias, pkcs12PassWord.toCharArray());
            KeyStore.PrivateKeyEntry privateKeyEntry;
            privateKeyEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(alias, new KeyStore.PasswordProtection(pkcs12PassWord.toCharArray()));
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }
    }

    public KeyPair getKeyPair(){

        try {
            Key privateKey = null;
            privateKey = ks.getKey(alias, pkcs12PassWord.toCharArray());
            Certificate cert = null;
            cert = ks.getCertificate(alias);
            PublicKey publicKey = cert.getPublicKey();
            KeyPair keys = new KeyPair(publicKey, (PrivateKey) privateKey);
            return keys;
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void test() throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, UnrecoverableKeyException {
        String password = pkcs12PassWord;

        KeyStore kspkcs12=ks;
        // kspkcs12.load(new FileInputStream("<file>.p12"),password.toCharArray());

        Key pk = null;
        Certificate cert = null;

        String alias = this.alias;
        if (kspkcs12.containsAlias(alias))
        {
            if (kspkcs12.isKeyEntry(alias))
            {
                pk = (PrivateKey)kspkcs12.getKey(alias, password.toCharArray());
                cert = kspkcs12.getCertificate(alias);
            }
        }

        if (pk == null || cert == null)
        {
            System.out.println("Can't find cert");
            return;
        }

        PrivateKey priv = (PrivateKey)pk;
        PublicKey pub = cert.getPublicKey();

        /* Create the cipher */
        Cipher rsaCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

        // Initialize the cipher for encryption
        rsaCipher.init(Cipher.ENCRYPT_MODE, pub);

        // Cleartext
        String cleartextin = "Hello World!!";
        byte[] clearbytesin = null;
        clearbytesin = cleartextin.getBytes();
        System.out.println("the original cleartext is: " + cleartextin);

        // Encrypt the cleartext
        byte[] cipherbytes = null;
        cipherbytes = rsaCipher.doFinal(clearbytesin);

        // Initialize the same cipher for decryption
        rsaCipher.init(Cipher.DECRYPT_MODE, priv);

        // Decrypt the ciphertext
        byte[] clearbytesout = rsaCipher.doFinal(cipherbytes);
        String cleartextout = new String(clearbytesout);
        System.out.println("the final cleartext is: " + cleartextout);
    }
}

