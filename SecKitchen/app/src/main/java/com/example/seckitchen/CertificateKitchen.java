package com.example.seckitchen;

import android.content.Context;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertPathParameters;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXCertPathValidatorResult;
import java.security.cert.PKIXParameters;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.util.Enumeration;



public class CertificateKitchen {
    private static final String TAG = CertificateKitchen.class.getSimpleName();
    public static final String PKCS12_FILENAME = "fred.p12";

    private Context c;
    public X509Certificate certificatesList[] = new X509Certificate[3];
    private Integer indexCertificateList = 0;
    public X509Certificate addCertificate509(String fName) {
        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X.509", "BC");
            Helpers h = new Helpers(c);
            InputStream in = h.getInputStream(fName);
            certificatesList[indexCertificateList] = (X509Certificate) cf.generateCertificate(in);
            printCertificateX509(certificatesList[indexCertificateList]);
            X509Certificate cert = certificatesList[indexCertificateList];
            indexCertificateList++;
            return cert;
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void printCertificateX509(X509Certificate cert){
        System.out.println("Subject DN : " + cert.getSubjectDN().getName());
        System.out.println("Issuer : " + cert.getIssuerDN().getName());
        System.out.println("Not After: " + cert.getNotAfter());
        System.out.println("Not Before: " + cert.getNotBefore());
        System.out.println("version: " + cert.getVersion());
        System.out.println("serial number : " + cert.getSerialNumber());

        PublicKey publicKey = cert.getPublicKey();
        System.out.println("PublicKey : \n" + publicKey);
        Log.d("","");
    }
    public CertificateKitchen(Context c ) {
            this.c = c;
            //this.certificatesList = new X509Certificate[];
    }


    public boolean validateChain(X509Certificate[] serverCertificates) {
        boolean res = true;
        if (null == serverCertificates) {
            if (null == certificatesList){
                res = false;
                return res;
            }
            serverCertificates = certificatesList;
        }

        X509Certificate currCertificate = null;
        X509Certificate prevCertificate =
                serverCertificates[serverCertificates.length - 1];

        for (int i = serverCertificates.length - 2; i >= 0; --i) {
            currCertificate = serverCertificates[i];
            String errorMessage;
            // if a certificate is null, we cannot verify the chain
            if (currCertificate == null) {
                Log.d("CerificateChainValidation", "null certificate in the chain");
                res = false;
                break;
            }

            if (!prevCertificate.getSubjectDN().equals(
                    currCertificate.getIssuerDN())) {
                errorMessage = "not trusted by chain";
                Log.e("CerificateChainValidation", errorMessage);
                res = false;
                break;
            }

            try {
                currCertificate.verify(prevCertificate.getPublicKey());
            } catch (GeneralSecurityException e) {
                errorMessage = e.getMessage();
                if (errorMessage == null) {
                    errorMessage = "not trusted by chain";
                }
                Log.e("CerificateChainValidation", errorMessage);
                res = false;
                break;
            }

            // verify if the dates are valid
            try {
                currCertificate.checkValidity();
            } catch (CertificateExpiredException e) {
                errorMessage = e.getMessage();
                if (errorMessage == null) {
                    errorMessage = "certificate expired";
                }
                Log.e("CerificateChainValidation", errorMessage);
                res = false;
                break;
            } catch (CertificateNotYetValidException e) {
                errorMessage = e.getMessage();
                if (errorMessage == null) {
                    errorMessage = "certificate not valid yet";
                }
                Log.e("CerificateChainValidation", errorMessage);
                res = false;
                break;
            }
            prevCertificate = currCertificate;
        }
        return res;
    }

    public void certificatePkcs12Kitchen() {
        InputStream in = null;
        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12", "BC");
            Helpers h = new Helpers(c);
            in = h.getInputStream("fred.p12");
            keyStore.load(in, "fred".toCharArray());
            Enumeration e = keyStore.aliases();
            String alias = "";
            while (e.hasMoreElements()) {
                alias = (String) e.nextElement();
                Log.d("get",PKCS12_FILENAME + " alias: " + alias);
                X509Certificate x509Cert = (X509Certificate) keyStore.getCertificate(alias);
                // addCertificateToKeyStore(c);
            }
            KeyStore.PrivateKeyEntry keyEntryFred = (KeyStore.PrivateKeyEntry) keyStore.getEntry(alias, null);
            X509Certificate cert = (X509Certificate) keyEntryFred.getCertificate();
            RSAPrivateKey privKey = (RSAPrivateKey) keyEntryFred.getPrivateKey();

            printCertificateX509(cert);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }
    /*
    public void getAliasFromPKCS12(){
        try {
            Enumeration e = ks.aliases();
            while (e.hasMoreElements()) {
                alias = (String) e.nextElement();
                Log.d("get",PKCS12_FILENAME + " alias: " + alias);
                X509Certificate x509Cert = (X509Certificate) keyStore.getCertificate(alias);
                // addCertificateToKeyStore(c);
            }
        } catch (Exception e) {}
    }
    */

    public void usingSystemTrustStore(){
        try {
            KeyStore ks = KeyStore.getInstance("AndroidCAStore");
            ks.load(null, null);
            Enumeration<String> aliases = null;

            aliases = ks.aliases();

            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                Log.d("usingSystemTrustStore", "Certificate alias: " +  alias);
                X509Certificate cert = null;
                cert = (X509Certificate) ks.getCertificate(alias);
                Log.d("usingSystemTrustStore", "Subject DN: " + cert.getSubjectDN().getName());
                Log.d("usingSystemTrustStore", "Issuer DN: " + cert.getIssuerDN().getName());
            }
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void stackoverflowExample(){
        try {
        final CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");

            byte[] certBytes = "pepe".getBytes();
            final X509Certificate certificateToCheck = (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(certBytes));
        // https://stackoverflow.com/questions/33493109/java-security-x509-certificate-verification-with-public-key

        final KeyStore trustStore;

            trustStore = KeyStore.getInstance("JKS");
            InputStream keyStoreStream = new FileInputStream("algo");
            trustStore.load(keyStoreStream, "your password".toCharArray());

            final CertPathBuilder certPathBuilder = CertPathBuilder.getInstance("PKIX");
            final X509CertSelector certSelector = new X509CertSelector();
            certSelector.setCertificate(certificateToCheck);

            final CertPathParameters certPathParameters = new PKIXBuilderParameters(trustStore, certSelector);
            final CertPathBuilderResult certPathBuilderResult = certPathBuilder.build(certPathParameters);
            final CertPath certPath = certPathBuilderResult.getCertPath();

            final CertPathValidator certPathValidator = CertPathValidator.getInstance("PKIX");
            final PKIXParameters validationParameters = new PKIXParameters(trustStore);
            validationParameters.setRevocationEnabled(true); // if you want to check CRL
            final X509CertSelector keyUsageSelector = new X509CertSelector();
            // to check digitalSignature and keyEncipherment bits
            keyUsageSelector.setKeyUsage(new boolean[] { true, false, true });
            validationParameters.setTargetCertConstraints(keyUsageSelector);
            final PKIXCertPathValidatorResult result = (PKIXCertPathValidatorResult)
                    certPathValidator.validate(certPath, validationParameters);

            System.out.println(result);

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertPathValidatorException e) {
            e.printStackTrace();
        } catch (CertPathBuilderException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

    }
}
