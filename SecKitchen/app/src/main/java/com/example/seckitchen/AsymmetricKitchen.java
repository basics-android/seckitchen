package com.example.seckitchen;

import android.content.Context;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class AsymmetricKitchen {
    private static final String TAG = AsymmetricKitchen.class.getSimpleName();
    private KeyPair     kpGenerator;
    private PrivateKey  privKey;
    private KeyPair     keyPair;
    private String      cipherProvider;
    private Context     context;
    private String      alias;
    private Cipher      cipher;
    public AsymmetricKitchen(KeyPair key, String provider, Context c){
        keyPair = key;
        cipherProvider = provider;
        context = c;
    }

    public AsymmetricKitchen(String aliasKey, String provider, Context c){
        alias = aliasKey;
        cipherProvider = provider;
        context = c;
    }



    public Cipher initCipher(Key Key, Integer mode, String algorithm){

        try {

            cipher = Cipher.getInstance(algorithm, cipherProvider);
            cipher.init(mode, Key);
            return cipher;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return null;
    }
    public void makeRsaCompleteBC(){
        try {
            byte[] input = new byte[] { (byte)0xbe, (byte)0xef };
            Cipher cipher = Cipher.getInstance("RSA/None/NoPadding", "BC");

            KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");
            RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(
                    new BigInteger("d46f473a2d746537de2056ae3092c451", 16), new BigInteger("11", 16));
            RSAPrivateKeySpec privKeySpec = new RSAPrivateKeySpec(
                    new BigInteger("d46f473a2d746537de2056ae3092c451", 16), new BigInteger("57791d5430d593164082036ad8b29fb1", 16));

            RSAPublicKey pubKey = (RSAPublicKey)keyFactory.generatePublic(pubKeySpec);
            RSAPrivateKey privKey = (RSAPrivateKey)keyFactory.generatePrivate(privKeySpec);

            cipher.init(Cipher.ENCRYPT_MODE, privKey);
            byte[] cipherText = new byte[0];

            cipherText = cipher.doFinal(input);

            cipher.init(Cipher.DECRYPT_MODE, pubKey );
            byte[] plainText = cipher.doFinal(cipherText);

        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }

    public byte[] makeRsaForSign (byte[] data) {


        try {
            Signature s = Signature.getInstance("SHA256withRSA/PSS");
            s.initSign(keyPair.getPrivate());
            s.update(data);
            // int l = s.sign(data,0,data.length);
            byte[] transformation= s.sign();
            return transformation;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }

        return null;
    }

    public byte[] makeRsaForDecrypt(Integer mode, byte[] data){

        initCipher(keyPair.getPrivate(), mode, "RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
        try {
            byte[] transformation = cipher.doFinal(data);
            System.out.println("transformation: " + Arrays.toString(transformation));
            return transformation;
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }
    public byte[] makeRsaForPublic(Integer mode,byte[] data){
        if (Cipher.DECRYPT_MODE == mode){
            initCipher(keyPair.getPublic(), mode, "RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
        }else{
            initCipher(keyPair.getPublic(), mode, "RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
        }


        try {
            byte[] transformation = cipher.doFinal(data);
            System.out.println("transformation: " + Arrays.toString(transformation));
            return transformation;
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }


}

