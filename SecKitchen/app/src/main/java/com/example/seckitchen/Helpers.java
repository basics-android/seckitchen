package com.example.seckitchen;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class Helpers {
    private static final String TAG = Helpers.class.getSimpleName();
    Context c;
    public Helpers(Context c ) {
        this.c = c;
    }
    public InputStream getInputStream(String fileName)
    {
        try {
            c.getAssets().open(fileName);
            //File file = new File(, fileName);
            InputStream fileInputStreamReader = c.getAssets().open(fileName);
            return fileInputStreamReader;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private static byte[] readContentIntoByteArray(File file)
    {
        FileInputStream fileInputStream = null;
        byte[] bFile = new byte[(int) file.length()];
        try
        {
            //convert file into array of bytes
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
            for (int i = 0; i < bFile.length; i++)
            {
                System.out.print((char) bFile[i]);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return bFile;
    }
    public void writeFile (String data, String fileName){
        try {
            File file = new File(c.getFilesDir(), fileName);

            FileOutputStream fileOutputStream = null;
            fileOutputStream = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter;
            outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(data, 0, data.length());
            bufferedWriter.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String readFile(String fileName){

        try {
            File file = new File(c.getFilesDir(), fileName);
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            InputStreamReader inputStreamReader = new InputStreamReader (fileInputStreamReader);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            long fLength = file.length();
            char[] data = new char[(int) fLength];
            String data2 = null;
            int len = bufferedReader.read(data);
            return Arrays.toString(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
