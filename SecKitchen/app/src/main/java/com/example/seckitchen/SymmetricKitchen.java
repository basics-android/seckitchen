package com.example.seckitchen;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;

public class SymmetricKitchen {
    private Context context;
    Cipher cipher;
    private SecretKey secretKey;
    byte[] encoded;
    byte[] iv;
    int tLeniv;
    String cipherProvider = "";
    private static final String TAG = SymmetricKitchen.class.getSimpleName();
    public SymmetricKitchen(SecretKey key, String provider, Context c){
        secretKey = key;
        cipherProvider = provider;
        context = c;
    }

    private void initCipher(SecretKey key, int mode, String algorithm){

        try {
            cipher = Cipher.getInstance(algorithm, cipherProvider);

            if (algorithm == "ARC4"){
                cipher.init(mode, key);
            }else {
                if (mode == Cipher.ENCRYPT_MODE) {
                    cipher.init(mode, key);
                    GCMParameterSpec ivParams = cipher.getParameters().getParameterSpec(GCMParameterSpec.class);
                    iv = ivParams.getIV();
                    tLeniv = ivParams.getTLen();
                } else {
                    GCMParameterSpec ivParams = new GCMParameterSpec(tLeniv, iv);
                    cipher.init(mode, key, ivParams);
                }
            }
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (InvalidParameterSpecException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }

    public byte[]  cipherFor (int encryptMode, byte[] data){
        return makeCipherFor (encryptMode, secretKey, data);
    }
    private byte[] makeCipherFor (int encryptMode, SecretKey key, byte[] rawMessageToBeDecode){
        initCipher( secretKey, encryptMode, "AES/GCM/NoPadding");
        try {
            byte[] transformation = cipher.doFinal(rawMessageToBeDecode);
            return transformation;
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void encrypt ( String content, String fileName) {
        initCipher( secretKey, Cipher.ENCRYPT_MODE, "AES/GCM/NoPadding");
        try {
            File file = new File(context.getFilesDir(), fileName);
            FileOutputStream fileOut = new FileOutputStream(file);
            CipherOutputStream cipherOut = new CipherOutputStream(fileOut, cipher);
            cipherOut.write(content.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String decrypt(String fileName) {
        String strDataDecode;
        initCipher( secretKey, Cipher.DECRYPT_MODE, "AES/GCM/NoPadding");
        try  {

            File file = new File(context.getFilesDir(), fileName);
            FileInputStream fileIn = new FileInputStream(file);

            CipherInputStream cipherIn = new CipherInputStream(fileIn, cipher);
            InputStreamReader inputReader = new InputStreamReader(cipherIn);
            byte[] data = new byte [(int)file.length()];
            int length = cipherIn.read(data,0, (int)file.length());
            strDataDecode = Arrays.toString(data);

            return strDataDecode;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] makeArc4For(int encryptMode, byte[] input){

        byte[] transformation = new byte[input.length];
        try {
            initCipher( secretKey, encryptMode,  "ARC4");
            int ctLength = 0;
            for (int i = 0; i< input.length;i++) {
                ctLength += cipher.update(input, i, 1, transformation, i);
            }
            ctLength += cipher.doFinal(transformation, ctLength);
            return transformation;
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (ShortBufferException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void rc4Complete(int encryptMode, String input){
        try {
            byte[] cipherText = new byte[input.length()];
            Cipher cipher = Cipher.getInstance("ARC4", "BC");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            int ctLength = cipher.update(input.getBytes(), 0, input.length(), cipherText, 0);
            ctLength += cipher.doFinal(cipherText, ctLength);
            System.out.println("cipher text: " + Arrays.toString(cipherText) + " bytes: " + ctLength);
            // decryption pass
            byte[] plainText = new byte[ctLength];
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            int ptLength = cipher.update(cipherText, 0, ctLength, plainText, 0);
            ptLength += cipher.doFinal(plainText, ptLength);
            System.out.println("plain text : " + Arrays.toString(plainText) + " bytes: " + ptLength);

        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (ShortBufferException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }

    }
    public void rc4CompleteLoop(int encryptMode, String input){
        try {
            byte[] cipherText = new byte[input.length()];
            Cipher cipher = Cipher.getInstance("ARC4", "BC");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            int ctLength = 0;
            for (int i = 0; i< input.getBytes().length;i++) {
                ctLength += cipher.update(input.getBytes(), i, 1, cipherText, i);
            }

            ctLength += cipher.doFinal(cipherText, ctLength);
            System.out.println("cipher text: " + Arrays.toString(cipherText) + " bytes: " + ctLength);
            // decryption pass
            byte[] plainText = new byte[ctLength];
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            int ptLength = 0;
            for (int i= 0; i< input.getBytes().length;i++) {
                ptLength += cipher.update(cipherText, i, 1, plainText, i);
            }
            ptLength += cipher.doFinal(plainText, ptLength);
            System.out.println("plain text : " + Arrays.toString(plainText) + " bytes: " + ptLength);

        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (ShortBufferException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }

    }
}
