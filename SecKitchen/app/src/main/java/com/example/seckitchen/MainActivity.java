package com.example.seckitchen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Arrays;

import javax.crypto.Cipher;

public class MainActivity extends AppCompatActivity {

    private final static String ENCRYPTED_FILE_NAME = "encryptedFile";
    private final static String PLAINTEXT_FILE_NAME = "plainTextFile";
    private final static String KEY_ALIAS = "kitchen00";
    private final static String DataForSave = "A world with Secrets is not a world, is a frightened world : ) but... smile and be kindness please!";

    ArrayList <KeyStoreKitchen> ksList = new ArrayList<KeyStoreKitchen>();
    private void  arc4BC(){
        GenKeysBC genBCKey = new GenKeysBC();
        SymmetricKitchen symmetricK;
        symmetricK = new SymmetricKitchen(genBCKey.forARC4(KEY_ALIAS, 256),"BC", getBaseContext());
        //symmetricK.rc4CompleteLoop(Cipher.ENCRYPT_MODE,DataForSave);

        byte[] bytesForSave = DataForSave.getBytes();
        System.out.println(" Data plain: "+ new String(bytesForSave, StandardCharsets.UTF_8));
        byte[] transformation = symmetricK.makeArc4For(Cipher.ENCRYPT_MODE, bytesForSave);
        System.out.println(" Data encripted (bytes): "+ Arrays.toString(transformation));
        byte[] reTransformation = symmetricK.makeArc4For(Cipher.DECRYPT_MODE, transformation);
        System.out.println(" Data decrypted: "+ new String(reTransformation, StandardCharsets.UTF_8));

    }
    private void aes4BC (){
            /*
            GenKeysAndroidStore generateKey = new GenKeysAndroidStore();
            // generateKey.forAES("vamosAver", 256);

             */
        GenKeysBC genBCKey = new GenKeysBC();
        SymmetricKitchen symmetricK;
        symmetricK = new SymmetricKitchen(genBCKey.forAES(KEY_ALIAS, 256),"BC", getBaseContext());
        //symmetricK = new SymmetricKitchen(generateKey.forAES("vamosAver", 256), "AndroidKeyStore");
        String msgOriginal = "this is the message for the people!!: Jump the systems!";
        Log.w("MainActivity", "..\n.\n msgOriginal :\n" + msgOriginal + "\n.\n.");
        Log.w("MainActivity", "..\n.\n msgOriginal Array :\n" + Arrays.toString(msgOriginal.getBytes()) + "\n.\n.");

        byte[] msgEncoded = symmetricK.cipherFor(Cipher.ENCRYPT_MODE, msgOriginal.getBytes());
        Log.w("MainActivity", "..\n.\n msgEncoded Array :\n" + Arrays.toString(msgEncoded) + "\n.\n.");
        byte[] msgDecoded = symmetricK.cipherFor(Cipher.DECRYPT_MODE, msgEncoded);
        Log.w("MainActivity", "..\n.\n msgDecoded Array :\n" + Arrays.toString(msgDecoded) + "\n.\n.");
        String s = new String(msgDecoded);
        Log.w("MainActivity", "..\n.\n msgDecoded :\n" + s + "\n.\n.");
        System.out.println(msgDecoded.toString());
    }
    private void cipherIOStreamBC (){
        GenKeysBC genBCKey = new GenKeysBC();
        SymmetricKitchen symmetricK;
        symmetricK = new SymmetricKitchen(genBCKey.forAES(KEY_ALIAS, 256),"BC", getBaseContext());
        Helpers helper = new Helpers(getBaseContext());

        helper.writeFile(DataForSave, PLAINTEXT_FILE_NAME);
        String dataForEncrypt = helper.readFile(PLAINTEXT_FILE_NAME);

        System.out.println(" Data from file plainTextFile: "+ dataForEncrypt);
        // dataForEncrypt = DataForSave;
        symmetricK.encrypt(dataForEncrypt.toString(), ENCRYPTED_FILE_NAME);
        String dataEncripted = helper.readFile(ENCRYPTED_FILE_NAME);
        System.out.println(" Data from file encryptedFileTextFile"+ dataEncripted);

        dataForEncrypt = symmetricK.decrypt(ENCRYPTED_FILE_NAME);
        System.out.println(" Data decrypted from file encryptedFileTextFile" + dataForEncrypt);
    }
    private void cipherRsa() {
        GenKeysAndroidStore genAndroidKeyStoreAssymK = new GenKeysAndroidStore();
        String alias = "cipherRsa";
        KeyPair keyPair = genAndroidKeyStoreAssymK.genKeyRSAForSigning(alias);
        AsymmetricKitchen asymmetricK = new AsymmetricKitchen(keyPair,"AndroidKeyStoreBCWorkaround", getApplicationContext());

        asymmetricK.makeRsaCompleteBC();

        System.out.println("Data plain text: " + DataForSave);
        byte[] data = DataForSave.getBytes();
        byte[] transformation = asymmetricK.makeRsaForPublic(Cipher.ENCRYPT_MODE, data);
        //byte[] decryptedData = asymmetricK.makeRsaForDecrypt(Cipher.DECRYPT_MODE, transformation);
        //System.out.println("Data decrypted text: " + Arrays.toString(decryptedData));
        byte[] signedData = asymmetricK.makeRsaForSign(data);
        byte[] decryptedDataWithPublicKey = asymmetricK.makeRsaForPublic(Cipher.DECRYPT_MODE, signedData);
        System.out.println("Data decrypted text: " + Arrays.toString(decryptedDataWithPublicKey));
    }
    private void certificateTrudAnchorKeytchen(){
        CertificateKitchen certificateK= new CertificateKitchen(getApplicationContext());
        certificateK.usingSystemTrustStore();
    }
    private void certificatePkcs12Keytchen(){
        CertificateKitchen certificateK= new CertificateKitchen(getApplicationContext());
        certificateK.certificatePkcs12Kitchen();
    }
    public void certificatex509Kitchen(){
        CertificateKitchen certificateK= new CertificateKitchen(getApplicationContext());
        certificateK.addCertificate509("fred.cer");
        certificateK.addCertificate509("signing-ca.cer");
        certificateK.addCertificate509("root-ca.cer");



        certificateK.validateChain(null);
    }

    private void certificateKeytchen() {
        Integer iOption = 2;
        switch (iOption) {
            case 0: {
                certificateTrudAnchorKeytchen();
            }
            case 1: {
                certificatePkcs12Keytchen();
            }
            case 2: {
                certificatex509Kitchen();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Integer iOption = 4;
        switch (iOption) {
            case 4:{
                certificateKeytchen();
            }
            break;
            case 3:{
                arc4BC();
            }
            break;
            case 2:{
                aes4BC();
            }
            break;
            case 1:{
                cipherIOStreamBC();
            }
            break;
            case 0:{
                cipherRsa();
            }
            break;
            default:{

            }
        }
    }
}