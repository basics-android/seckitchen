package com.example.seckitchen;

import android.content.Context;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyInfo;
import android.security.keystore.KeyProperties;
import android.util.Log;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Calendar;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.security.auth.x500.X500Principal;

public class GenKeysAndroidStore {
    private static final String TAG = GenKeysAndroidStore.class.getSimpleName();
    public static final String ANDROID_KEYSTORE = "AndroidKeyStore";

    public static final String AES = KeyProperties. KEY_ALGORITHM_AES;

    public static final String HMAC_SHA1 = KeyProperties.KEY_ALGORITHM_HMAC_SHA1;

    /** Keyed-Hash Message Authentication Code (HMAC) key using SHA-224 as the hash. */
    public static final String HMAC_SHA224 = KeyProperties.KEY_ALGORITHM_HMAC_SHA224;

    /** Keyed-Hash Message Authentication Code (HMAC) key using SHA-256 as the hash. */
    public static final String HMAC_SHA256 = KeyProperties.KEY_ALGORITHM_HMAC_SHA256;

    /** Keyed-Hash Message Authentication Code (HMAC) key using SHA-384 as the hash. */
    public static final String HMAC_SHA384 = KeyProperties.KEY_ALGORITHM_HMAC_SHA384;

    /** Keyed-Hash Message Authentication Code (HMAC) key using SHA-512 as the hash. */
    public static final String HMAC_SHA512 = KeyProperties.KEY_ALGORITHM_HMAC_SHA512;

    public GenKeysAndroidStore(){

    }

    private final static int KEY_SIZE_AES256 = 256;


    public SecretKey forAES (String alias, int size){
        return genSymmetricKey(AES, createAESKeyGenParam (alias, size));
    }

    /* Todo
    public SecretKey genHMAC_SHA1 (String alias, int size){
        return genSymmetricKey(HMAC_SHA1, createAESKeyGenParam (alias, size));
        }
    public SecretKey genHMAC_SHA224 (String alias, int size){
        return genSymmetricKey(HMAC_SHA224, createAESKeyGenParam (alias, size));
    }
    public SecretKey genHMAC_SHA256 (String alias, int size){
        return genSymmetricKey(HMAC_SHA256, createAESKeyGenParam (alias, size)); }
    public SecretKey genHMAC_SHA384 (String alias, int size){
        return genSymmetricKey(HMAC_SHA384, createAESKeyGenParam (alias, size));
    }
    public SecretKey genHMAC_SHA512 (String alias, int size){
        return genSymmetricKey(HMAC_SHA512, createAESKeyGenParam (alias, size));
    }
    */



    private KeyGenParameterSpec createAESKeyGenParam(
            String keyAlias, int size) {
        KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(
                keyAlias,
                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .setKeySize(size);
        return builder.build();
    }

    private SecretKey genSymmetricKey(String algorithm,  KeyGenParameterSpec kgSpec){

        try {
            KeyGenerator keyGenerator = null;
            keyGenerator = KeyGenerator.getInstance(
                    algorithm, "AndroidKeyStore");
            keyGenerator.init(    kgSpec);
            SecretKey key = keyGenerator.generateKey();

            SecretKeyFactory factory = SecretKeyFactory.getInstance(key.getAlgorithm(), "AndroidKeyStore");
            KeyInfo keyInfo;

            keyInfo = (KeyInfo) factory.getKeySpec(key, KeyInfo.class);
            System.out.println("is this the default key size?: " + keyInfo.getKeySize());
            System.out.println("is this the getKeystoreAlias()?: " + keyInfo.getKeystoreAlias());
            System.out.println("is this the getBlockModes()()?: " + Arrays.toString(keyInfo.getBlockModes()));
            System.out.println("is this the getBlockModes()()?: " + keyInfo.getOrigin());

            return key;

        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void generateARC4Key(){
        KeyGenerator keyGenerator = null;
        try {
            keyGenerator = KeyGenerator.getInstance(
                    "ARC4", "AndroidKeyStore");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }

    private KeyGenParameterSpec createAES256GCMKeyGenParameterSpec(String keyAlias, int size){
        return createAESKeyGenParam(
                keyAlias, KEY_SIZE_AES256);
    }




    public KeyPair genKeyRsaCertificate(String alias, Integer years) {
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        // expires 1 year from today
        end.add(years, Calendar.YEAR);

        try {
            KeyPairGenerator gen = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
            KeyGenParameterSpec spec = new KeyGenParameterSpec.Builder(
                    alias,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT | KeyProperties.PURPOSE_SIGN)
                    .setCertificateSubject(new X500Principal("CN=" + alias))
                    .setKeyValidityStart(start.getTime())
                    .setKeyValidityEnd(end.getTime())
                    .setCertificateSerialNumber(BigInteger.TEN)
                    .build();
            gen.initialize(spec);
            KeyPair kpGenerator = gen.generateKeyPair();
            return kpGenerator;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return null;
    }

    public KeyPair genEcKeysForSignVerify(String alias)  {

        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_EC, ANDROID_KEYSTORE);
            kpg.initialize(new KeyGenParameterSpec.Builder(
                    alias,
                    KeyProperties.PURPOSE_SIGN |
                            KeyProperties.PURPOSE_VERIFY |
                            KeyProperties.PURPOSE_SIGN)
                    .setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
                    .build());
            KeyPair kp = kpg.generateKeyPair();
            return kp;
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return null;
    }

    public KeyPair genKeyRSAECBOAEPWithSHA256AndMGF1Padding(String alias){
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");
            keyPairGenerator.initialize(
                    new KeyGenParameterSpec.Builder(
                            alias,
                            KeyProperties.PURPOSE_DECRYPT | KeyProperties.PURPOSE_ENCRYPT)
                            .setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
                            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_OAEP)
                            .build());
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            return keyPair;
//            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
//            cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return null;
    }
    public KeyPair genKeyRSAForSigning(String alias){

        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");
            keyPairGenerator.initialize(
                    new KeyGenParameterSpec.Builder(
                            "key1",
                            KeyProperties.PURPOSE_SIGN | KeyProperties.PURPOSE_DECRYPT)
                            .setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
                            .setSignaturePaddings(KeyProperties.SIGNATURE_PADDING_RSA_PSS)
                            .build());
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            return keyPair;
//            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
//            cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return null;
    }

}
