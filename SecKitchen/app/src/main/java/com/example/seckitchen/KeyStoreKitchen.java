package com.example.seckitchen;

import android.content.Context;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.security.keystore.WrappedKeyEntry;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;


public class KeyStoreKitchen {
    private static KeyStoreKitchen instance;
    private static final String TAG = KeyStoreKitchen.class.getSimpleName();
    private KeyStoreKitchen(){
        addKeyStoreType();
        ksList = new ArrayList<>();
        for (String k: keyStoreTypesList){
            initKeyStore(k);
        }
    }

    public static KeyStoreKitchen getInstance (){
        if (instance == null)
            instance = new KeyStoreKitchen();
        return instance;
    }

    public enum KEY_STORE_TYPE{
        AndroidCAStore,
        ANDROIDKEYSTORE,
        BCPKCS12,
        BKS,
        BouncyCastle,
        PKCS12,
        PKCS12_DEF,
        DEFAULTKEYSTORE,
        JKS,
        MAX_KEY_STORE_TYPE,
    }
    private static ArrayList <String> keyStoreTypesList = new ArrayList<String>();
    private void addKeyStoreType(){
        keyStoreTypesList.add("AndroidCAStore"); // Provider Name: HarmonyJSSE - Provider Info: Harmony JSSE Provider
        keyStoreTypesList.add("AndroidKeyStore");
        keyStoreTypesList.add("BCPKCS12"); // available Apis 1-8
        keyStoreTypesList.add("BKS"); // Bouncy Castle keystore which is deprecated for most applications
        keyStoreTypesList.add("BouncyCastle"); // BC // BouncyCastle Security Provider v1.57.
        keyStoreTypesList.add("PKCS12"); // BC // BouncyCastle Security Provider v1.57 it is old
        keyStoreTypesList.add("PKCS12-DEF"); // available Apis 1-8
        keyStoreTypesList.add(KeyStore.getDefaultType());
        keyStoreTypesList.add("JKS");
        
    }

    private static ArrayList <KeyStore> ksList;

    public KeyStore getKeyStoreProvider(KEY_STORE_TYPE ksType){
        return ksList.get(ksType.ordinal());
    }

    private String LogTag = "KeyStoreKitchen";

    private StringBuilder   ProviderInfo;
    private Provider        ksProvider;
    public static final String ANDROID_KEYSTORE = "AndroidKeyStore";

    KEY_STORE_TYPE currentKeyStoreProvider = KEY_STORE_TYPE.DEFAULTKEYSTORE;

    private KeyStore defaultKeyStore(){
        return ksList.get(KEY_STORE_TYPE.ANDROIDKEYSTORE.ordinal());
    }
    private KeyStore androidKeyStore(){
        return ksList.get(KEY_STORE_TYPE.ANDROIDKEYSTORE.ordinal());
    }

    private void initKeyStore(String ksName){
        try {
            if (ksName == null || ksName == "")
                defaultKeyStore();
            else {
                Log.w(LogTag, "Init keystore Type " + ksName);
                KeyStore ks = KeyStore.getInstance(ksName);
                getProviderInfo(ks);
                ksList.add(ks);
            }
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

    }
    private void listBCCapabilities()    {
        System.out.println("#func listBCCapabilities");
        Provider	provider = Security.getProvider("BC");
        Iterator it = provider.keySet().iterator();
        while (it.hasNext())
        {
            String	entry = (String)it.next();
            // this indicates the entry refers to another entry
            if (entry.startsWith("Alg.Alias."))
            {
                entry = entry.substring("Alg.Alias.".length());
            }
            String  factoryClass = entry.substring(0, entry.indexOf('.'));
            String  name = entry.substring(factoryClass.length() + 1);

            System.out.println(factoryClass + ": " + name);
        }
    }
    public Provider getProviderInfo (KeyStore ks){
        ksProvider = ks.getProvider();

        ProviderInfo = new StringBuilder();
        ProviderInfo
                .append(".\n\n")
                .append("   getType: " + ks.getType()).append("\n")
                .append("   Provider Name: " + ksProvider.getName()).append("\n")
                .append("   Provider Info: " + ksProvider.getInfo()).append("\n\n");

        Log.w(LogTag, ".\n.\nCurrent KeyStore Provider:" + ProviderInfo + "End Provider Info\n.\n");

        return  ksProvider;
    }

    private void getKeyPublicKey(String alias){
        KeyStore ks = null;
        try {
            ks = ksList.get(KEY_STORE_TYPE.ANDROIDKEYSTORE.ordinal());
            ks.load(null);
            Key kp = (Key) ks.getKey(alias, null);
            System.out.println(kp);

        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }

    }

    private void getPrivateKey(String alias){
        KeyStore ks = null;
        try {
            ks = ksList.get(KEY_STORE_TYPE.ANDROIDKEYSTORE.ordinal());

            ks.load(null);
            KeyStore.Entry entry = ks.getEntry(alias, null);
            if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
                Log.w("TAG", "Not an instance of a PrivateKeyEntry");
                return ;
            }
//            Signature s = Signature.getInstance("SHA256withECDSA");
//            byte[] data = "new byte[10]".getBytes();
//            s.update(data);
//            byte[] signature = s.sign();
//            s.initSign(((KeyStore.PrivateKeyEntry) entry).getPrivateKey());
        } catch ( NoSuchAlgorithmException | CertificateException | IOException e) {
            e.printStackTrace();
        } catch (KeyStoreException | UnrecoverableEntryException e) {
            e.printStackTrace();
        }


    }

    private void enumerateEntries(){
        try {
            KeyStore ks = ksList.get(KEY_STORE_TYPE.ANDROIDKEYSTORE.ordinal());
            ks.load(null);
            Enumeration<String> aliases = ks.aliases();

            for (String e = aliases.nextElement(); aliases.hasMoreElements();)
                System.out.println(e);
            while (aliases.hasMoreElements()){
                Log.w("enumerateEntries", aliases.nextElement());
            }
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException | KeyStoreException e) {
            e.printStackTrace();
        }
    }

private static final String ALIAS = "yourkey";
@RequiresApi(api = Build.VERSION_CODES.P)
public void importSecretKey (byte[] wrappedKey, String wrappingKeyAlias) throws Exception {
    KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
    keyStore.load(null, null);
    AlgorithmParameterSpec spec = new KeyGenParameterSpec.Builder(wrappingKeyAlias,
            KeyProperties.PURPOSE_WRAP_KEY)
            .setDigests(KeyProperties.DIGEST_SHA256)
            .build();
    KeyStore.Entry wrappedKeyEntry = new WrappedKeyEntry(wrappedKey, wrappingKeyAlias,
            "RSA/ECB/OAEPPadding", spec);
    keyStore.setEntry(ALIAS, wrappedKeyEntry, null);
}




    public PrivateKey loadPrivteKey(String alias, KeyStore ks) throws Exception {
        if (!ks.isKeyEntry(alias)) {
            Log.e("TAG", "Could not find key alias: " + alias);
            return null;
        }
        KeyStore.Entry entry = ks.getEntry(alias, null);
        if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
            Log.e("loadPrivteKey", " alias: " + alias + " is not a PrivateKey");
            return null;
        }
        PrivateKey privKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();
        return privKey;
    }

public void useTheKey(){
    // Use Key

    try {
        KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
        keyStore.load(null, null);
        Key key = null;
        key = keyStore.getKey(ALIAS, null);

        Cipher c = Cipher.getInstance("AES/ECB/PKCS7Padding");
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encrypted = c.doFinal("hello, world".getBytes());
        c = Cipher.getInstance("AES/ECB/PKCS7Padding");
        c.init(Cipher.DECRYPT_MODE, key);
    } catch (CertificateException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
    } catch (NoSuchPaddingException e) {
        e.printStackTrace();
    } catch (InvalidKeyException e) {
        e.printStackTrace();
    } catch (IllegalBlockSizeException e) {
        e.printStackTrace();
    } catch (BadPaddingException e) {
        e.printStackTrace();
    }       catch (KeyStoreException e) {
        e.printStackTrace();
    } catch (UnrecoverableKeyException e) {
        e.printStackTrace();
    }
}

    public KeyPair loadKeyPair(String alias){
        try {
            KeyStore ks = ksList.get(KEY_STORE_TYPE.ANDROIDKEYSTORE.ordinal());
            PrivateKey privateKey = (PrivateKey) ks.getKey(alias, null);
            PublicKey publicKey = ks.getCertificate(alias).getPublicKey();
            KeyPair k = new KeyPair(publicKey, privateKey);
            return k;
            // Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
            // cipher.init(Cipher.DECRYPT_MODE, privateKey);

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }
        return null;
    }
}
