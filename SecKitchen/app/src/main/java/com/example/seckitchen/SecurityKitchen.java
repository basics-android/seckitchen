package com.example.seckitchen;

import android.util.Log;

import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class SecurityKitchen {
    private static final String TAG = SymmetricKitchen.class.getSimpleName();
    private static SecurityKitchen instance;
    private ArrayList<Provider> providers;
    private ArrayList<String> providersNames;
    private SecurityKitchen(){
        providers = new ArrayList<Provider>();
        providersNames = new ArrayList<String>();
        getSecProviders();
    }

    public static SecurityKitchen getInstance(){
        if (instance == null) {
            instance = new SecurityKitchen();
        }
        return instance;
    }

    public ArrayList<Provider> getProvidersList (){
        return providers;
    }

    public ArrayList<String> getProvidersNames(){
        return providersNames;
    }

    private void getSecProviders() {
        //Collections.addAll(providers, Security.getProviders());
        //Collections.addAll(providersNames, providers
        for (Provider provider : Security.getProviders()) {
            providers.add(provider);
            providersNames.add(provider.getName());
            Log.w("SecurityKitchen", "***************************************");
            Log.w("SecurityKitchen", "provider.getName(): " +provider.getName());
            Set<Provider.Service> ss = provider.getServices();
            for (Provider.Service s : ss ){

                Log.w("SecurityKitchen", "***************************************");
                Log.w("SecurityKitchen", "s.getAlgorithm(); " +s.getAlgorithm());
                // Log.w("SecurityKitchen", "s.getAttribute(); " +s.getAttribute();
                Log.w("SecurityKitchen", "s.getClassName(); " +s.getClassName());
                Log.w("SecurityKitchen", "s.getType(); " +s.getType());
                Log.w("SecurityKitchen", "s.getProvider(); " +s.getProvider());
                Log.w("SecurityKitchen", "s.getAttribute(); " +s.getAttribute(""));
            }
            Log.w("SecurityKitchen", "***************************************");
        }
    }

private void getCiphersProviders() {
    for (Provider provider : Security.getProviders()) {
        Log.w("SecurityKitchen", "***************************************");
        Log.w("SecurityKitchen", "Ciphers for Provider: " +provider.getName());
        Set<Provider.Service> ss = provider.getServices();
        Integer total = 0;
        for (Provider.Service s : ss ){
            if (s.getType().contains("Cipher")){
                Log.w("SecurityKitchen", "    Algorithm: " + s.getAlgorithm());
                total++;
            }
        }
        if (0 == total){
            Log.w("SecurityKitchen", "    No Algorithm for this provider: ");
        }
        Log.w("SecurityKitchen", "***************************************");
    }
}

    public String getProviderName(Provider p){
        if (p != null) {
            return p.getName();
        }
        else {
            return "null provider!";
        }
    }
}
